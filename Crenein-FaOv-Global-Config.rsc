##----------------Global Configuration----------------##
#V4.0
#
##Notifications.
:global notificationglobalenabled 1;
:global devicename "Identity";
###Telegram config.
:global telegrambot "token";
:global telegramchatid "chat-id";
##Probes.
:global globalprobesenabled 1;
###Traffic probe.
:global globalminimumbandwidth 3000000;
###ICMP probe.
:global globalicmpprobeserver "1.0.0.1";
###HTTP/s probe.
:global globalhttpfetchurl "http://example.com/index.html";
:global globalhttpfetchip "93.184.216.34";
:global globalhttpfetchmode "http";
##Events.
:global eventscriptglobalenabled 1;
##DNSs
:global globaldnscheck "telegram.org"
:global globalprimarydnss "10.10.10.10,10.10.8.11"
:global globalsecundarydnss "10.10.10.10,10.10.8.11"
