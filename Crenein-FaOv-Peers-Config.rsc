##----------------Peers Configurations----------------##
#V4.1
#
:global peer1 {
  pname="IZ";
  penabled=1;
  pifname="ether1";
  pdefaultroutecomment="W1-R1-N1-RE";
  proutingmark="W1-R1-N1-RE";
  pprobescript="Crenein-FaOv-Default-Probe";
  pnotificationenable=1;
  pnotificationscript="Crenein-FaOv-Default-Notification";
  peventscriptenable=1;
  pupeventscript="Crenein-FaOv-Default-UpEvent";
  pdowneventscript="Crenein-FaOv-Default-DownEvent";
  ppeerstatus=1;
  pactiondelay=2;
  pdownscount=0;
  pupscount=0;
}
:global peer2 {
  pname="Compus";
  penabled=1;
  pifname="ether2";
  pdefaultroutecomment="W2-R1-N1-RE";
  proutingmark="W2-R1-N1-RE";
  pprobescript="Crenein-FaOv-Default-Probe";
  pnotificationenable=1;
  pnotificationscript="Crenein-FaOv-Default-Notification";
  peventscriptenable=1;
  pupeventscript="Crenein-FaOv-Default-UpEvent";
  pdowneventscript="Crenein-FaOv-Default-DownEvent";
  ppeerstatus=1;
  pactiondelay=2;
  pdownscount=0;
  pupscount=0;
}
#------------------------------------#
