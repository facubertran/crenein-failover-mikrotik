##----------------Events----------------##
#V4.1
#
##----------------Enable routes----------------##
:global eventscriptglobalenabled
:if ($eventscriptglobalenabled = 1) do={
  :global routingmark
  :global defaultroutecomment
  /ip route enable [/ip route find routing-mark=$routingmark]
  /ip route enable [/ip route find comment=$defaultroutecomment]
  ##-----------------System-Note-Notification-----------------##
  /system note set note="0";
}
