##----------------Remember Notification----------------##
#V4.0
#
:global notificationglobalenabled
if ($notificationglobalenabled = 1) do={
  ##-----------------Log-Notification-----------------##
  :log warning "Recordatorio: Existe al menos un peer caido."
  ##-----------------Telegram-Notification-----------------##
  :global telegrambot
  :global telegramchatid
  /tool fetch url="https://api.telegram.org/bot$telegrambot/sendMessage\?chat_id=$telegramchatid&text=Recordatorio: Existe al menos un peer caido." keep-result=no
}
