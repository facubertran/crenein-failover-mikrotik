##----------------Default-Probe----------------##
#V4.1
#
##----------------Variables de atributos----------------##
:global globalprobesenabled
:if ($globalprobesenabled = 1) do={
  ##----------------Variables de atributos----------------##
  :global globalicmpprobeserver
  :global defaultroutecomment
  :global globalhttpfetchurl
  :global globalhttpfetchip
  :global globalhttpfetchmode
  :global globalminimumbandwidth
  :global devicename
  :global peername
  :global ifname
  :global gateway
  :global routingmark
  ##-----------------Global-Probes-Status-----------------##
  :global faovdefaultprobesstatus
  :global faovdefaultprobesmessage
  :set faovdefaultprobesstatus 1;
  :set faovdefaultprobesmessage "";
  :global httpfetcherror
  ##---------------------IFRun-Probe----------------------##
  :local ifrunn [interface get $ifname running]
  :if ($ifrunn = false) do={
    :set $faovdefaultprobesstatus 0;
    :set faovdefaultprobesmessage "Peer $peername --> FAIL (IFRun-Probe --> failed) on $devicename";
    :put "IFRun-Probe --> FAIL";
  } else={
    :set $faovdefaultprobesstatus 1;
    :set faovdefaultprobesmessage "Peer $peername --> OK (IFRun-Probe --> ok) on $devicename";
    :put "IFRun-Probe --> OK";
  }
  ##---------------------Gateway-Probe----------------------##
  :if ($faovdefaultprobesstatus = 1) do={
    :foreach gwfind in=[/ip route find comment=$defaultroutecomment] do={
      :local gwstatus [/ip route get $gwfind gateway-status]
      :if ($gwstatus~"unreachable") do={
        :set faovdefaultprobesstatus 0;
        :set faovdefaultprobesmessage "Peer $peername --> FAIL (Gateway-Probe --> failed) on $devicename";
        :put "Gateway-Probe --> FAIL";
      } else={
        :put "Gateway-Probe --> OK";
      }
    }
  }
  ##---------------------Traffic-Probe----------------------##
  :if ($faovdefaultprobesstatus = 1) do={
    :local trafficmonitor [/interface monitor-traffic $ifname as-value once];
    :put ("Traffic-Probe result is " . ($trafficmonitor->"rx-bits-per-second"));
      :if (($trafficmonitor->"rx-bits-per-second") < $globalminimumbandwidth) do={
        :put "Traffic-Probe --> FAIL";
        ##---------------------ICMP-Probe----------------------##
        /ip route set [/ip route find comment="CreneinFaOvRoute"] dst-address=$globalicmpprobeserver gateway=$gateway;
        :local floodping [/tool flood-ping $globalicmpprobeserver count=10 size=64 as-value];
        :put ("ICMP-Probe result is "."(sent --> ".($floodping->"sent")." received --> ".($floodping->"received")." avg-rtt --> ".($floodping->"avg-rtt").")");
          :if (($floodping->"received") <= 8) do={
              :put "Traffic-ICMP probes --> FAIL";
              ##---------------------HTTP-Probe----------------------##
              :set $httpfetcherror 0;
              /ip route set [/ip route find comment="CreneinFaOvRoute"] dst-address=$globalhttpfetchip gateway=$gateway;
              :do {/tool fetch url="$globalhttpfetchurl" mode="$globalhttpfetchmode" as-value} on-error={
                :set $httpfetcherror 1;
              }
              :if ($httpfetcherror = 1) do={
                :set faovdefaultprobesstatus 0;
                :set faovdefaultprobesmessage "Peer $peername --> FAIL (Traffic-ICMP-HTTP probes --> failed) on $devicename";
                :put "Traffic-ICMP-HTTP probes --> FAIL";
              } else={
                :set $faovdefaultprobesstatus 1;
                :set faovdefaultprobesmessage "Peer $peername --> OK (HTTP-Probe --> ok) on $devicename";
                :put "HTTP-Probe --> OK";
              }
            } else={
            :set $faovdefaultprobesstatus 1;
            :set faovdefaultprobesmessage "Peer $peername --> OK (ICMP-Probe --> ok) on $devicename";
            :put "ICMP-Probe --> OK";
            }
        } else={
          :set $faovdefaultprobesstatus 1;
          :set faovdefaultprobesmessage "Peer $peername --> OK (Traffic-Probe --> ok) on $devicename";
          :put "Traffic-Probe --> OK";
        }
      }
    }
