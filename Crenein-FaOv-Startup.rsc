#Execute data scripts.
[/system script run "Crenein-FaOv-Global-Config"];
[/system script run "Crenein-FaOv-Peers-Config"];
[/system script run "Crenein-FaOv-DNS-Probe"];
#--------------------------------------------#
:global peer1
:global peer2
:local peers {$peer1;$peer2};
#------------------------------------------#
#V4.2
#Vars.
:global ifname
:global defaultroutecomment
:global gateway
:global routingmark
:global peername
:global faovdefaultprobesstatus
#
:foreach check in=$peers do={
	##-----------------Dynamic-Gateway-Update------------------##
	:set $gateway [/ip route get [/ip route find routing-mark=($check->"proutingmark")] gateway];
	#Peer info.
	:put ("----------------------Peer-Config.-----------------------");
	:put ("Peer name ". "--> " .($check->"pname"));
	:put ("Peer enabled ". "--> " .($check->"penabled"));
	:put ("Interface name ". "--> " .($check->"pifname"));
	:put ("Default route comment ". "--> " .($check->"pdefaultroutecomment"));
	:put ("Gateway ". "--> " .($check->"pgateway"));
	:put ("Routing mark ". "--> " .($check->"proutingmark"));
	:put ("Probe Script ". "--> " .($check->"pprobescript"));
	:put ("Notification Script ". "--> " .($check->"pnotificationscript"));
	:put ("UpEvent Script ". "--> " .($check->"pupeventscript"));
	:put ("DownEvent Script ". "--> " .($check->"pdowneventscript"));
	:put (". . . . . . . . . . . . Peer-Status . . . . . . . . . . .");
	:put ("Peer status ". "--> " .($check->"ppeerstatus"));
	:put ("Action delay ". "--> " .($check->"pactiondelay"));
	:put ("Downs counts ". "--> " .($check->"pdownscount"));
	:put ("Ups counts ". "--> " .($check->"pupscount"));
	:put ("............................................");
	##Probes.
	:if (($check->"penabled") = 1) do={
		#Set vars.
		:set ifname ($check->"pifname");
		:set defaultroutecomment ($check->"pdefaultroutecomment");
		:set routingmark ($check->"proutingmark");
		:set peername ($check->"pname");
		#Runn probe script.
		[/system script run ($check->"pprobescript")];
		#Probe fail action.
		:if ($faovdefaultprobesstatus = 0) do={
			:set ($check->"pdownscount") (($check->"pdownscount") + 1);
				:if (($check->"pdownscount") >= ($check->"pactiondelay")) do={
					:set ($check->"ppeerstatus") 0;
					:if (($check->"peventscriptenable") = 1) do={
						[/system script run ($check->"pdowneventscript")]
						}
					:if (($check->"pnotificationenable") = 1) do={
						[/system script run ($check->"pnotificationscript")]
					}
				}
		}
		#Probe ok action.
		:if ($faovdefaultprobesstatus = 1) do={
				:set ($check->"pupscount") (($check->"pupscount") + 1);
				:if (($check->"pupscount") >= ($check->"pactiondelay")) do={
					:set ($check->"pupscount") 0;
					:set ($check->"pdownscount") 0;
					:set ($check->"ppeerstatus") 1;
					:if (($check->"peventscriptenable") = 1) do={
						[/system script run ($check->"pupeventscript")]
						}
					:if (($check->"pnotificationenable") = 1) do={
						[/system script run ($check->"pnotificationscript")]
					}
				}
		}
	:put ("");
	} else={
		:put ("Peer ". ($check->"pname"). " disabled");
	}
}
