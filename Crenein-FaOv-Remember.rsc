##----------------Remember Script----------------##
#V4.0
#
#Remember-PeerDown
:global peer1
:global peer2
:local peers {$peer1;$peer2};
#------------------------------------------#
#V4.0
#
:foreach check in=$peers do={
	#Peer info.
	:put ("-------------------".($check->"pname")."-------------------");
	:put ("Peer name ". "--> " .($check->"pname"));
	:put ("Interface name ". "--> " .($check->"pifname"));
	:put ("Peer status ". "--> " .($check->"ppeerstatus"));
	:put ("Downs counts ". "--> " .($check->"pdownscount"));
	:put ("............................................");
	##Probes.
	:if (($check->"ppeerstatus") = 0) do={
		#Runn notification script.
		:if (($check->"pnotificationenable") = 1) do={
			[/system script run Crenein-FaOv-Remember-Notification]
		}
	}
}
