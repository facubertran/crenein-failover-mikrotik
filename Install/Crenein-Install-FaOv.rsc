#V4.0.2
/system script
add name=Crenein-FailOver source="#\
    Execute data scripts.\r\
    \n[/system script run \"Crenein-FaOv-Global-Config\"];\r\
    \n[/system script run \"Crenein-FaOv-DNS-Probe\"];\r\
    \n[/system script run \"Crenein-FaOv-Peers-Config\"];\r\
    \n#--------------------------------------------#\r\
    \n:global peer1\r\
    \n:global peer2\r\
    \n:local peers {\$peer1;\$peer2};\r\
    \n#------------------------------------------#\r\
    \n#V4.0.3\r\
    \n#Vars.\r\
    \n:global ifname\r\
    \n:global defaultroutecomment\r\
    \n:global gateway\r\
    \n:global routingmark\r\
    \n:global peername\r\
    \n:global faovdefaultprobesstatus\r\
    \n#\r\
    \n:foreach check in=\$peers do={\r\
    \n\t##-----------------Dynamic-Gateway-Update------------------##\r\
    \n  :set \$gateway [/ip route get [/ip route find routing-mark=(\$check->\
    \"proutingmark\")] gateway];\r\
    \n\t#Peer info.\r\
    \n\t:put (\"--------------------\".(\$check->\"pname\").\"-Config.--------\
    -------------\");\r\
    \n\t:put (\"Peer name \". \"--> \" .(\$check->\"pname\"));\r\
    \n\t:put (\"Peer enabled \". \"--> \" .(\$check->\"penabled\"));\r\
    \n\t:put (\"Interface name \". \"--> \" .(\$check->\"pifname\"));\r\
    \n\t:put (\"Default route comment \". \"--> \" .(\$check->\"pdefaultroutec\
    omment\"));\r\
    \n\t:put (\"Gateway \". \"--> \" .\$gateway);\r\
    \n\t:put (\"Routing mark \". \"--> \" .(\$check->\"proutingmark\"));\r\
    \n\t:put (\"Probe Script \". \"--> \" .(\$check->\"pprobescript\"));\r\
    \n\t:put (\"Notification Script \". \"--> \" .(\$check->\"pnotificationscr\
    ipt\"));\r\
    \n\t:put (\"UpEvent Script \". \"--> \" .(\$check->\"pupeventscript\"));\r\
    \n\t:put (\"DownEvent Script \". \"--> \" .(\$check->\"pdowneventscript\")\
    );\r\
    \n\t:put (\". . . . . . . . . \".(\$check->\"pname\").\"-Status . . . . . \
    . . . . .\");\r\
    \n\t:put (\"Peer status \". \"--> \" .(\$check->\"ppeerstatus\"));\r\
    \n\t:put (\"Action delay \". \"--> \" .(\$check->\"pactiondelay\"));\r\
    \n\t:put (\"Downs counts \". \"--> \" .(\$check->\"pdownscount\"));\r\
    \n\t:put (\"Ups counts \". \"--> \" .(\$check->\"pupscount\"));\r\
    \n\t:put (\"............................................\");\r\
    \n\t##Probes.\r\
    \n\t:if ((\$check->\"penabled\") = 1) do={\r\
    \n\t\t#Set vars.\r\
    \n\t\t:set ifname (\$check->\"pifname\");\r\
    \n\t\t:set defaultroutecomment (\$check->\"pdefaultroutecomment\");\r\
    \n\t\t:set routingmark (\$check->\"proutingmark\");\r\
    \n\t\t:set peername (\$check->\"pname\");\r\
    \n\t\t#Runn probe script.\r\
    \n\t\t[/system script run (\$check->\"pprobescript\")];\r\
    \n\t\t#Probe fail action.\r\
    \n\t\t:if (\$faovdefaultprobesstatus = 0) do={\r\
    \n\t\t\t:set (\$check->\"pdownscount\") ((\$check->\"pdownscount\") + 1);\
    \r\
    \n\t\t\t:if ((\$check->\"pdownscount\") >= (\$check->\"pactiondelay\")) do\
    ={\r\
    \n\t\t\t\t:if ((\$check->\"ppeerstatus\") = 1) do={\r\
    \n\t\t\t\t\t:set (\$check->\"ppeerstatus\") 0;\r\
    \n\t\t\t\t\t:if ((\$check->\"peventscriptenable\") = 1) do={\r\
    \n\t\t\t\t\t\t[/system script run (\$check->\"pdowneventscript\")]\r\
    \n\t\t\t\t\t\t}\r\
    \n\t\t\t\t\t:if ((\$check->\"pnotificationenable\") = 1) do={\r\
    \n\t\t\t\t\t\t[/system script run (\$check->\"pnotificationscript\")]\r\
    \n\t\t\t\t\t}\r\
    \n\t\t\t\t}\r\
    \n\t\t\t}\r\
    \n\t\t}\r\
    \n\t\t#Probe ok action.\r\
    \n\t\t:if (\$faovdefaultprobesstatus = 1) do={\r\
    \n\t\t\t:if ((\$check->\"ppeerstatus\") = 0) do={\r\
    \n\t\t\t\t:set (\$check->\"pupscount\") ((\$check->\"pupscount\") + 1);\r\
    \n\t\t\t\t:if ((\$check->\"pupscount\") >= (\$check->\"pactiondelay\")) do\
    ={\r\
    \n\t\t\t\t\t:set (\$check->\"pupscount\") 0;\r\
    \n\t\t\t\t\t:set (\$check->\"pdownscount\") 0;\r\
    \n\t\t\t\t\t:set (\$check->\"ppeerstatus\") 1;\r\
    \n\t\t\t\t\t:if ((\$check->\"peventscriptenable\") = 1) do={\r\
    \n\t\t\t\t\t\t[/system script run (\$check->\"pupeventscript\")]\r\
    \n\t\t\t\t\t\t}\r\
    \n\t\t\t\t\t:if ((\$check->\"pnotificationenable\") = 1) do={\r\
    \n\t\t\t\t\t\t[/system script run (\$check->\"pnotificationscript\")]\r\
    \n\t\t\t\t\t}\r\
    \n\t\t\t\t}\r\
    \n\t\t\t}\r\
    \n\t\t}\r\
    \n\t:put (\"\");\r\
    \n\t} else={\r\
    \n\t\t:put (\"Peer \". (\$check->\"pname\"). \" disabled\");\r\
    \n\t}\r\
    \n}\r\
    \n\r\
    \n"
add name=Crenein-FaOv-Default-DownEvent source="#\
    #----------------Disable routes----------------##\r\
    \n#V4.1\r\
    \n#\r\
    \n:global eventscriptglobalenabled\r\
    \n:if (\$eventscriptglobalenabled = 1) do={\r\
    \n  :global routingmark\r\
    \n  :global defaultroutecomment\r\
    \n  /ip route disable [/ip route find routing-mark=\$routingmark]\r\
    \n  /ip route disable [/ip route find comment=\$defaultroutecomment]\r\
    \n  ##-----------------System-Note-Notification-----------------##\r\
    \n  /system note set note=\"1\";\r\
    \n}\r\
    \n"
add name=Crenein-FaOv-Default-Notification source="#\
    #----------------Notifications----------------##\r\
    \n#V4.2\r\
    \n#\r\
    \n:global notificationglobalenabled\r\
    \nif (\$notificationglobalenabled = 1) do={\r\
    \n  ##-----------------Global-Notification-----------------##\r\
    \n  :global faovdefaultprobesmessage\r\
    \n  ##-----------------Log-Notification-----------------##\r\
    \n  :log warning \"\$faovdefaultprobesmessage\"\r\
    \n  ##-----------------Telegram-Notification-----------------##\r\
    \n  :global telegrambot\r\
    \n  :global telegramchatid\r\
    \n  /tool fetch url=\"https://api.telegram.org/bot\$telegrambot/sendMessag\
    e\\\?chat_id=\$telegramchatid&text=\$faovdefaultprobesmessage\" keep-resul\
    t=no\r\
    \n  ##---------------------File-Report-----------------------##\r\
    \n  #/file print file=Crenein-FailOver-log.txt\r\
    \n  :local date [/system clock get date];\r\
    \n  :local time [/system clock get time];\r\
    \n  :local filecontent [/file get Crenein-FailOver-Status.txt contents]\r\
    \n  :local newfilecontent (\"\$filecontent\\r\".\"\\n\$date-\".\"\$time \"\
    .\"\$faovdefaultprobesmessage\")\r\
    \n  /file set Crenein-FailOver-Status.txt contents=\$newfilecontent\r\
    \n}\r\
    \n"
add name=Crenein-FaOv-Default-Probe source="##----------------Default-Probe----------------##\r\
  \n#V4.1\r\
  \n#\r\
  \n##----------------Variables de atributos----------------##\r\
  \n:global globalprobesenabled\r\
  \nif (\$globalprobesenabled = 1) do={\r\
  \n  ##----------------Variables de atributos----------------##\r\
  \n  :global globalicmpprobeserver\r\
  \n  :global defaultroutecomment\r\
  \n  :global globalhttpfetchurl\r\
  \n  :global globalhttpfetchip\r\
  \n  :global globalhttpfetchmode\r\
  \n  :global globalminimumbandwidth\r\
  \n  :global devicename\r\
  \n  :global peername\r\
  \n  :global ifname\r\
  \n  :global gateway\r\
  \n  :global routingmark\r\
  \n  ##-----------------Global-Probes-Status-----------------##\r\
  \n  :global faovdefaultprobesstatus\r\
  \n  :global faovdefaultprobesmessage\r\
  \n  :set faovdefaultprobesstatus 1;\r\
  \n  :set faovdefaultprobesmessage \"\";\r\
  \n  :global httpfetcherror\r\
  \n  ##---------------------IFRun-Probe----------------------##\r\
  \n\r\
  \n  :local ifrunn [interface get \$ifname running]\r\
  \n  :if (\$ifrunn = false) do={\r\
  \n    :set \$faovdefaultprobesstatus 0;\r\
  \n    :set faovdefaultprobesmessage \"Peer \$peername --> FAIL (IFRun-Prob\
  e --> failed) on \$devicename\";\r\
  \n    :put \"IFRun-Probe --> FAIL\";\r\
  \n  } else={\r\
  \n    :set \$faovdefaultprobesstatus 1;\r\
  \n    :set faovdefaultprobesmessage \"Peer \$peername --> OK (IFRun-Probe \
  --> ok) on \$devicename\";\r\
  \n    :put \"IFRun-Probe --> OK\";\r\
  \n  }\r\
  \n  ##---------------------Gateway-Probe----------------------##\r\
  \n  :if (\$faovdefaultprobesstatus = 1) do={\r\
  \n    :foreach gwfind in=[/ip route find comment=\$defaultroutecomment] do\
  ={\r\
  \n      :local gwstatus [/ip route get \$gwfind gateway-status]\r\
  \n      :if (\$gwstatus~\"unreachable\") do={\r\
  \n        :set faovdefaultprobesstatus 0;\r\
  \n        :set faovdefaultprobesmessage \"Peer \$peername --> FAIL (Gatewa\
  y-Probe --> failed) on \$devicename\";\r\
  \n        :put \"Gateway-Probe --> FAIL\";\r\
  \n      } else={\r\
  \n        :put \"Gateway-Probe --> OK\";\r\
  \n      }\r\
  \n    }\r\
  \n  }\r\
  \n  ##---------------------Traffic-Probe----------------------##\r\
  \n  :if (\$faovdefaultprobesstatus = 1) do={\r\
  \n    :local trafficmonitor [/interface monitor-traffic \$ifname as-value \
  once];\r\
  \n    :put (\"Traffic-Probe result is \" . (\$trafficmonitor->\"rx-bits-pe\
  r-second\"));\r\
  \n      :if ((\$trafficmonitor->\"rx-bits-per-second\") < \$globalminimumb\
  andwidth) do={\r\
  \n        :put \"Traffic-Probe --> FAIL\";\r\
  \n        ##---------------------ICMP-Probe----------------------##\r\
  \n        /ip route set [/ip route find comment=\"CreneinFaOvRoute\"] dst-\
  address=\$globalicmpprobeserver gateway=\$gateway;\r\
  \n        :local floodping [/tool flood-ping \$globalicmpprobeserver count\
  =10 size=64 as-value];\r\
  \n        :put (\"ICMP-Probe result is \".\"(sent --> \".(\$floodping->\"s\
  ent\").\" received --> \".(\$floodping->\"received\").\" avg-rtt --> \".(\
  \$floodping->\"avg-rtt\").\")\");\r\
  \n          :if ((\$floodping->\"received\") <= 8) do={\r\
  \n              :put \"Traffic-ICMP probes --> FAIL\";\r\
  \n              ##---------------------HTTP-Probe----------------------##\
  \r\
  \n              :set \$httpfetcherror 0;\r\
  \n              /ip route set [/ip route find comment=\"CreneinFaOvRoute\"\
  ] dst-address=\$globalhttpfetchip gateway=\$gateway;\r\
  \n              :do {/tool fetch url=\"\$globalhttpfetchurl\" mode=\"\$glo\
  balhttpfetchmode\" as-value} on-error={\r\
  \n                :set \$httpfetcherror 1;\r\
  \n              }\r\
  \n              :if (\$httpfetcherror = 1) do={\r\
  \n                :set faovdefaultprobesstatus 0;\r\
  \n                :set faovdefaultprobesmessage \"Peer \$peername --> FAIL\
  \_(Traffic-ICMP-HTTP probes --> failed) on \$devicename\";\r\
  \n                :put \"Traffic-ICMP-HTTP probes --> FAIL\";\r\
  \n              } else={\r\
  \n                :set \$faovdefaultprobesstatus 1;\r\
  \n                :set faovdefaultprobesmessage \"Peer \$peername --> OK (\
  HTTP-Probe --> ok) on \$devicename\";\r\
  \n                :put \"HTTP-Probe --> OK\";\r\
  \n              }\r\
  \n            } else={\r\
  \n            :set \$faovdefaultprobesstatus 1;\r\
  \n            :set faovdefaultprobesmessage \"Peer \$peername --> OK (ICMP\
  -Probe --> ok) on \$devicename\";\r\
  \n            :put \"ICMP-Probe --> OK\";\r\
  \n            }\r\
  \n        } else={\r\
  \n          :set \$faovdefaultprobesstatus 1;\r\
  \n          :set faovdefaultprobesmessage \"Peer \$peername --> OK (Traffi\
  c-Probe --> ok) on \$devicename\";\r\
  \n          :put \"Traffic-Probe --> OK\";\r\
  \n        }\r\
  \n      }\r\
  \n    }\r\
  \n"
add name=Crenein-FaOv-Default-UpEvent source="##----------------Events----------------##\r\
    \n#V4.1\r\
    \n#\r\
    \n##----------------Enable routes----------------##\r\
    \n:global eventscriptglobalenabled\r\
    \n:if (\$eventscriptglobalenabled = 1) do={\r\
    \n  :global routingmark\r\
    \n  :global defaultroutecomment\r\
    \n  /ip route enable [/ip route find routing-mark=\$routingmark]\r\
    \n  /ip route enable [/ip route find comment=\$defaultroutecomment]\r\
    \n  ##-----------------System-Note-Notification-----------------##\r\
    \n  /system note set note=\"0\";\r\
    \n}\r\
    \n"
add name=Crenein-FaOv-Global-Config source="##----------------Global Configuration----------------##\r\
    \n#V4.0\r\
    \n#\r\
    \n##Notifications.\r\
    \n:global notificationglobalenabled 1;\r\
    \n:global devicename \"Identity\";\r\
    \n###Telegram config.\r\
    \n:global telegrambot \"token\";\r\
    \n:global telegramchatid \"chat-id\";\r\
    \n##Probes.\r\
    \n:global globalprobesenabled 1;\r\
    \n###Traffic probe.\r\
    \n:global globalminimumbandwidth 3000000;\r\
    \n###ICMP probe.\r\
    \n:global globalicmpprobeserver \"1.0.0.1\";\r\
    \n###HTTP/s probe.\r\
    \n:global globalhttpfetchurl \"http://example.com/index.html\";\r\
    \n:global globalhttpfetchip \"93.184.216.34\";\r\
    \n:global globalhttpfetchmode \"http\";\r\
    \n##Events.\r\
    \n:global eventscriptglobalenabled 1;\r\
    \n##DNSs\r\
    \n:global globaldnscheck \"telegram.org\"\r\
    \n:global globalprimarydnss \"8.8.8.8\"\r\
    \n:global globalsecundarydnss \"8.8.8.8,1.1.1.1\"\r\
    \n"
add name=Crenein-FaOv-Peers-Config source="##----------------Peers Configurations----------------##\r\
    \n#V4.1\r\
    \n#\r\
    \n:global peer1 {\r\
    \n  pname=\"IZ\";\r\
    \n  penabled=1;\r\
    \n  pifname=\"ether1\";\r\
    \n  pdefaultroutecomment=\"W1-R1-N1-RE\";\r\
    \n  proutingmark=\"W1-R1-N1-RE\";\r\
    \n  pprobescript=\"Crenein-FaOv-Default-Probe\";\r\
    \n  pnotificationenable=1;\r\
    \n  pnotificationscript=\"Crenein-FaOv-Default-Notification\";\r\
    \n  peventscriptenable=1;\r\
    \n  pupeventscript=\"Crenein-FaOv-Default-UpEvent\";\r\
    \n  pdowneventscript=\"Crenein-FaOv-Default-DownEvent\";\r\
    \n  ppeerstatus=1;\r\
    \n  pactiondelay=2;\r\
    \n  pdownscount=0;\r\
    \n  pupscount=0;\r\
    \n}\r\
    \n:global peer2 {\r\
    \n  pname=\"Compus\";\r\
    \n  penabled=1;\r\
    \n  pifname=\"ether2\";\r\
    \n  pdefaultroutecomment=\"W2-R1-N1-RE\";\r\
    \n  proutingmark=\"W2-R1-N1-RE\";\r\
    \n  pprobescript=\"Crenein-FaOv-Default-Probe\";\r\
    \n  pnotificationenable=1;\r\
    \n  pnotificationscript=\"Crenein-FaOv-Default-Notification\";\r\
    \n  peventscriptenable=1;\r\
    \n  pupeventscript=\"Crenein-FaOv-Default-UpEvent\";\r\
    \n  pdowneventscript=\"Crenein-FaOv-Default-DownEvent\";\r\
    \n  ppeerstatus=1;\r\
    \n  pactiondelay=2;\r\
    \n  pdownscount=0;\r\
    \n  pupscount=0;\r\
    \n}\r\
    \n#------------------------------------#\r\
    \n"
add name=Crenein-FaOv-DNS-Probe source="##---------------------DNS-Probe----------------------##\r\
    \n#V4.0\r\
    \n#\r\
    \n:global globaldnscheck\r\
    \n:global globalprimarydnss\r\
    \n:global globalsecundarydnss\r\
    \n:global globaldnsstatus\r\
    \n[/ip dns set server=\$globalprimarydnss];\r\
    \n:set globaldnsstatus 1;\r\
    \n:do {[:resolve \$globaldnscheck]} on-error={\r\
    \n  :set globaldnsstatus 0;\r\
    \n}\r\
    \n:if (\$globaldnsstatus = 0) do={\r\
    \n  :log warning \"DNS-Probe -> FAIL\";\r\
    \n  [/ip dns set server=\$globalsecundarydnss];\r\
    \n}\r\
    \n"
add name=Crenein-FaOv-ResTASA-Probe source="##----------------Probe TASA Residential----------------##\r\
    \n#V4.0\r\
    \n#\r\
    \n##----------------Variables de atributos----------------##\r\
    \n:global globalprobesenabled\r\
    \nif (\$globalprobesenabled = 1) do={\r\
    \n  ##----------------Variables de atributos----------------##\r\
    \n  :global globalicmpprobeserver\r\
    \n  :global defaultroutecomment\r\
    \n  :global globalhttpfetchurl\r\
    \n  :global globalhttpfetchip\r\
    \n  :global globalhttpfetchmode\r\
    \n  :global globalminimumbandwidth\r\
    \n  :global devicename\r\
    \n  :global peername\r\
    \n  :global ifname\r\
    \n  :global gateway\r\
    \n  :global routingmark\r\
    \n  ##-----------------Global-Probes-Status-----------------##\r\
    \n  :global faovdefaultprobesstatus\r\
    \n  :global faovdefaultprobesmessage\r\
    \n  :set faovdefaultprobesstatus 1;\r\
    \n  :set faovdefaultprobesmessage \"\";\r\
    \n  :global httpfetcherror\r\
    \n  ##---------------------IFRun-Probe----------------------##\r\
    \n\r\
    \n  :local ifrunn [interface get \$ifname running]\r\
    \n  :if (\$ifrunn = false) do={\r\
    \n    :set \$faovdefaultprobesstatus 0;\r\
    \n    :set faovdefaultprobesmessage \"Peer \$peername --> FAIL (IFRun-Prob\
    e --> failed) on \$devicename\";\r\
    \n    :put \"IFRun-Probe --> FAIL\";\r\
    \n  } else={\r\
    \n    :set \$faovdefaultprobesstatus 1;\r\
    \n    :set faovdefaultprobesmessage \"Peer \$peername --> OK (IFRun-Probe \
    --> ok) on \$devicename\";\r\
    \n    :put \"IFRun-Probe --> OK\";\r\
    \n  }\r\
    \n  ##---------------------Gateway-Probe----------------------##\r\
    \n  :if (\$faovdefaultprobesstatus = 1) do={\r\
    \n    :foreach gwfind in=[/ip route find comment=\$defaultroutecomment] do\
    ={\r\
    \n      :local gwstatus [/ip route get \$gwfind gateway-status]\r\
    \n      :if (\$gwstatus~\"unreachable\") do={\r\
    \n        :set faovdefaultprobesstatus 0;\r\
    \n        :set faovdefaultprobesmessage \"Peer \$peername --> FAIL (Gatewa\
    y-Probe --> failed) on \$devicename\";\r\
    \n        :put \"Gateway-Probe --> FAIL\";\r\
    \n      } else={\r\
    \n        :put \"Gateway-Probe --> OK\";\r\
    \n      }\r\
    \n    }\r\
    \n  }\r\
    \n  ##---------------------Traffic-Probe----------------------##\r\
    \n  :if (\$faovdefaultprobesstatus = 1) do={\r\
    \n    :local trafficmonitor [/interface monitor-traffic \$ifname as-value \
    once];\r\
    \n    :put (\"Traffic-Probe result is \" . (\$trafficmonitor->\"rx-bits-pe\
    r-second\"));\r\
    \n      :if ((\$trafficmonitor->\"rx-bits-per-second\") < \$globalminimumb\
    andwidth) do={\r\
    \n        :put \"Traffic-Probe --> FAIL\";\r\
    \n        ##---------------------HTTP-Probe----------------------##\r\
    \n        /ip route set [/ip route find comment=\"CreneinFaOvRoute\"] dst-\
    address=\$globalhttpfetchip gateway=\$gateway;\r\
    \n        :do {/tool fetch url=\"\$globalhttpfetchurl\" mode=\"\$globalhtt\
    pfetchmode\" as-value} on-error={\r\
    \n          :set \$httpfetcherror 1;\r\
    \n        }\r\
    \n        :if (\$httpfetcherror = 1) do={\r\
    \n          :put \"Traffic-HTTP probes --> FAIL\";\r\
    \n          ##---------------------ICMP-Probe----------------------##\r\
    \n          /ip route set [/ip route find comment=\"CreneinFaOvRoute\"] ds\
    t-address=\$globalicmpprobeserver gateway=\$gateway;\r\
    \n          :local floodping [/tool flood-ping \$globalicmpprobeserver cou\
    nt=10 size=64 as-value];\r\
    \n          :put (\"ICMP-Probe result is \".\"(sent --> \".(\$floodping->\
    \"sent\").\" received --> \".(\$floodping->\"received\").\" avg-rtt --> \"\
    .(\$floodping->\"avg-rtt\").\")\");\r\
    \n          :if ((\$floodping->\"received\") <= 8) do={\r\
    \n              :put \"Traffic-HTTP-ICMP probes --> FAIL\";\r\
    \n                :set faovdefaultprobesstatus 0;\r\
    \n                :set faovdefaultprobesmessage \"Peer \$peername --> FAIL\
    \_(Traffic-HTTP-ICMP probes --> failed) on \$devicename\";\r\
    \n                :put \"Traffic-HTTP-ICMP probes --> FAIL\";\r\
    \n              } else={\r\
    \n                :set \$faovdefaultprobesstatus 1;\r\
    \n                :set faovdefaultprobesmessage \"Peer \$peername --> OK (\
    ICMP-Probe --> ok) on \$devicename\";\r\
    \n                :put \"ICMP-Probe --> OK\";\r\
    \n              }\r\
    \n            } else={\r\
    \n            :set \$faovdefaultprobesstatus 1;\r\
    \n            :set faovdefaultprobesmessage \"Peer \$peername --> OK (HTTP\
    -Probe --> ok) on \$devicename\";\r\
    \n            :put \"HTTP-Probe --> OK\";\r\
    \n            }\r\
    \n        } else={\r\
    \n          :set \$faovdefaultprobesstatus 1;\r\
    \n          :set faovdefaultprobesmessage \"Peer \$peername --> OK (Traffi\
    c-Probe --> ok) on \$devicename\";\r\
    \n          :put \"Traffic-Probe --> OK\";\r\
    \n        }\r\
    \n      }\r\
    \n    }\r\
    \n"
add name=Crenein-FaOv-Remember-Notification source="#\
    #----------------Remember Notification----------------##\r\
    \n#V4.0\r\
    \n#\r\
    \n:global notificationglobalenabled\r\
    \nif (\$notificationglobalenabled = 1) do={\r\
    \n  ##-----------------Log-Notification-----------------##\r\
    \n  :log warning \"Recordatorio: Existe al menos un peer caido.\"\r\
    \n  ##-----------------Telegram-Notification-----------------##\r\
    \n  :global telegrambot\r\
    \n  :global telegramchatid\r\
    \n  /tool fetch url=\"https://api.telegram.org/bot\$telegrambot/sendMessag\
    e\\\?chat_id=\$telegramchatid&text=Recordatorio: Existe al menos un peer c\
    aido.\" keep-result=no\r\
    \n}\r\
    \n"
add name=Crenein-FaOv-Remember source="##----------------Remember Script----------------##\r\
    \n#V4.0\r\
    \n#\r\
    \n#Remember-PeerDown\r\
    \n:global peer1\r\
    \n:global peer2\r\
    \n:local peers {\$peer1;\$peer2};\r\
    \n#------------------------------------------#\r\
    \n#V4.0\r\
    \n#\r\
    \n:foreach check in=\$peers do={\r\
    \n\t#Peer info.\r\
    \n\t:put (\"-------------------\".(\$check->\"pname\").\"-----------------\
    --\");\r\
    \n\t:put (\"Peer name \". \"--> \" .(\$check->\"pname\"));\r\
    \n\t:put (\"Interface name \". \"--> \" .(\$check->\"pifname\"));\r\
    \n\t:put (\"Peer status \". \"--> \" .(\$check->\"ppeerstatus\"));\r\
    \n\t:put (\"Downs counts \". \"--> \" .(\$check->\"pdownscount\"));\r\
    \n\t:put (\"............................................\");\r\
    \n\t##Probes.\r\
    \n\t:if ((\$check->\"ppeerstatus\") = 0) do={\r\
    \n\t\t#Runn notification script.\r\
    \n\t\t:if ((\$check->\"pnotificationenable\") = 1) do={\r\
    \n\t\t\t[/system script run Crenein-FaOv-Remember-Notification]\r\
    \n\t\t}\r\
    \n\t}\r\
    \n}\r\
    \n"
add name=Crenein-FaOv-Startup source="#\
        Execute data scripts.\r\
        \n[/system script run \"Crenein-FaOv-Global-Config\"];\r\
        \n[/system script run \"Crenein-FaOv-Peers-Config\"];\r\
        \n[/system script run \"Crenein-FaOv-DNS-Probe\"];\r\
        \n#--------------------------------------------#\r\
        \n:global peer1\r\
        \n:global peer2\r\
        \n:local peers {\$peer1;\$peer2};\r\
        \n#------------------------------------------#\r\
        \n#V4.2\r\
        \n#Vars.\r\
        \n:global ifname\r\
        \n:global defaultroutecomment\r\
        \n:global gateway\r\
        \n:global routingmark\r\
        \n:global peername\r\
        \n:global faovdefaultprobesstatus\r\
        \n#\r\
        \n:foreach check in=\$peers do={\r\
        \n\t##-----------------Dynamic-Gateway-Update------------------##\r\
        \n\t:set \$gateway [/ip route get [/ip route find routing-mark=(\$check->\
        \"proutingmark\")] gateway];\r\
        \n\t#Peer info.\r\
        \n\t:put (\"----------------------Peer-Config.-----------------------\");\
        \r\
        \n\t:put (\"Peer name \". \"--> \" .(\$check->\"pname\"));\r\
        \n\t:put (\"Peer enabled \". \"--> \" .(\$check->\"penabled\"));\r\
        \n\t:put (\"Interface name \". \"--> \" .(\$check->\"pifname\"));\r\
        \n\t:put (\"Default route comment \". \"--> \" .(\$check->\"pdefaultroutec\
        omment\"));\r\
        \n\t:put (\"Gateway \". \"--> \" .(\$check->\"pgateway\"));\r\
        \n\t:put (\"Routing mark \". \"--> \" .(\$check->\"proutingmark\"));\r\
        \n\t:put (\"Probe Script \". \"--> \" .(\$check->\"pprobescript\"));\r\
        \n\t:put (\"Notification Script \". \"--> \" .(\$check->\"pnotificationscr\
        ipt\"));\r\
        \n\t:put (\"UpEvent Script \". \"--> \" .(\$check->\"pupeventscript\"));\r\
        \n\t:put (\"DownEvent Script \". \"--> \" .(\$check->\"pdowneventscript\")\
        );\r\
        \n\t:put (\". . . . . . . . . . . . Peer-Status . . . . . . . . . . .\");\
        \r\
        \n\t:put (\"Peer status \". \"--> \" .(\$check->\"ppeerstatus\"));\r\
        \n\t:put (\"Action delay \". \"--> \" .(\$check->\"pactiondelay\"));\r\
        \n\t:put (\"Downs counts \". \"--> \" .(\$check->\"pdownscount\"));\r\
        \n\t:put (\"Ups counts \". \"--> \" .(\$check->\"pupscount\"));\r\
        \n\t:put (\"............................................\");\r\
        \n\t##Probes.\r\
        \n\t:if ((\$check->\"penabled\") = 1) do={\r\
        \n\t\t#Set vars.\r\
        \n\t\t:set ifname (\$check->\"pifname\");\r\
        \n\t\t:set defaultroutecomment (\$check->\"pdefaultroutecomment\");\r\
        \n\t\t:set routingmark (\$check->\"proutingmark\");\r\
        \n\t\t:set peername (\$check->\"pname\");\r\
        \n\t\t#Runn probe script.\r\
        \n\t\t[/system script run (\$check->\"pprobescript\")];\r\
        \n\t\t#Probe fail action.\r\
        \n\t\t:if (\$faovdefaultprobesstatus = 0) do={\r\
        \n\t\t\t:set (\$check->\"pdownscount\") ((\$check->\"pdownscount\") + 1);\
        \r\
        \n\t\t\t\t:if ((\$check->\"pdownscount\") >= (\$check->\"pactiondelay\")) \
        do={\r\
        \n\t\t\t\t\t:set (\$check->\"ppeerstatus\") 0;\r\
        \n\t\t\t\t\t:if ((\$check->\"peventscriptenable\") = 1) do={\r\
        \n\t\t\t\t\t\t[/system script run (\$check->\"pdowneventscript\")]\r\
        \n\t\t\t\t\t\t}\r\
        \n\t\t\t\t\t:if ((\$check->\"pnotificationenable\") = 1) do={\r\
        \n\t\t\t\t\t\t[/system script run (\$check->\"pnotificationscript\")]\r\
        \n\t\t\t\t\t}\r\
        \n\t\t\t\t}\r\
        \n\t\t}\r\
        \n\t\t#Probe ok action.\r\
        \n\t\t:if (\$faovdefaultprobesstatus = 1) do={\r\
        \n\t\t\t\t:set (\$check->\"pupscount\") ((\$check->\"pupscount\") + 1);\r\
        \n\t\t\t\t:if ((\$check->\"pupscount\") >= (\$check->\"pactiondelay\")) do\
        ={\r\
        \n\t\t\t\t\t:set (\$check->\"pupscount\") 0;\r\
        \n\t\t\t\t\t:set (\$check->\"pdownscount\") 0;\r\
        \n\t\t\t\t\t:set (\$check->\"ppeerstatus\") 1;\r\
        \n\t\t\t\t\t:if ((\$check->\"peventscriptenable\") = 1) do={\r\
        \n\t\t\t\t\t\t[/system script run (\$check->\"pupeventscript\")]\r\
        \n\t\t\t\t\t\t}\r\
        \n\t\t\t\t\t:if ((\$check->\"pnotificationenable\") = 1) do={\r\
        \n\t\t\t\t\t\t[/system script run (\$check->\"pnotificationscript\")]\r\
        \n\t\t\t\t\t}\r\
        \n\t\t\t\t}\r\
        \n\t\t}\r\
        \n\t:put (\"\");\r\
        \n\t} else={\r\
        \n\t\t:put (\"Peer \". (\$check->\"pname\"). \" disabled\");\r\
        \n\t}\r\
        \n}\r\
        \n"

/system scheduler
add disabled=yes interval=1m name=Crenein-FailOver on-event=Crenein-FailOver start-date=\
    apr/07/2019 start-time=12:56:26
add disabled=yes interval=6h name=Crenein-FaOv-Remember on-event=Crenein-FaOv-Remember start-date=\
    apr/08/2019 start-time=12:00:00
add disabled=yes name=Crenein-FaOv-Startup on-event=Crenein-FaOv-Startup start-time=startup

/ip route add comment=CreneinFaOvRoute distance=1 dst-address=1.0.0.1/32 gateway=1.1.1.1
/file print file=Crenein-FailOver-log.txt
