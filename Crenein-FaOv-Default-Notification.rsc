##----------------Notifications----------------##
#V4.2
#
:global notificationglobalenabled
if ($notificationglobalenabled = 1) do={
  ##-----------------Global-Notification-----------------##
  :global faovdefaultprobesmessage
  ##-----------------Log-Notification-----------------##
  :log warning "$faovdefaultprobesmessage"
  ##-----------------Telegram-Notification-----------------##
  :global telegrambot
  :global telegramchatid
  /tool fetch url="https://api.telegram.org/bot$telegrambot/sendMessage\?chat_id=$telegramchatid&text=$faovdefaultprobesmessage" keep-result=no
  ##---------------------File-Report-----------------------##
  :local date [/system clock get date];
  :local time [/system clock get time];
  :local filecontent [/file get Crenein-FailOver-log.txt contents]
  :local newfilecontent ("$filecontent\r"."\n$date-"."$time "."$faovdefaultprobesmessage")
  /file set Crenein-FailOver-Status.txt contents=$newfilecontent
}
