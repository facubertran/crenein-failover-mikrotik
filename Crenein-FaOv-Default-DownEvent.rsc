##----------------Disable routes----------------##
#V4.1
#
:global eventscriptglobalenabled
:if ($eventscriptglobalenabled = 1) do={
  :global routingmark
  :global defaultroutecomment
  /ip route disable [/ip route find routing-mark=$routingmark]
  /ip route disable [/ip route find comment=$defaultroutecomment]
  ##-----------------System-Note-Notification-----------------##
  /system note set note="1";
}
