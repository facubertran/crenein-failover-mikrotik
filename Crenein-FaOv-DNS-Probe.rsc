##---------------------DNS-Probe----------------------##
#V4.0
#
:global globaldnscheck
:global globalprimarydnss
:global globalsecundarydnss
:global globaldnsstatus
[/ip dns set server=$globalprimarydnss];
:set globaldnsstatus 1;
:do {[:resolve $globaldnscheck]} on-error={
  :set globaldnsstatus 0;
}
:if ($globaldnsstatus = 0) do={
  :log warning "DNS-Probe -> FAIL";
  [/ip dns set server=$globalsecundarydnss];
}
